@if (count($displayData))
    @foreach ($displayData as $record)
        <tr>
            @foreach ($record as $field)
                <td>{!! $field !!}</td>
            @endforeach
            <td>1</td>
        </tr>
    @endforeach
    <tr>
        <td colspan="{{count($columns) - 1}}" align="right" style="background-color: #fcfcfc; border:0px">Total Outstanding</td>
        <td>2</td>
    </tr>
    <tr>
        <td colspan="{{count($columns) - 1}}" align="right" style="background-color: #fcfcfc; border:0px">Total Due</td>
        <td>2</td>
    </tr>
@else
    <tr>
        <td colspan="10" style="text-align: center">{{ trans('texts.empty_table') }}</td>
    </tr>
@endif