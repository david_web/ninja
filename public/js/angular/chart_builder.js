var dependencies = [ 
    'angularUtils.directives.dirPagination'
    ];

var app = angular.module('app', dependencies );

app.controller('ReportController',
            ['$scope','$rootScope','$http' ,
    function($scope ,  $rootScope , $http ){
        $http.get('/ajax-report')
            .then(function(response) {
                $scope.displayData = response.data.displayData;
                $scope.record = response.data.record;
                console.log($scope.displayData)
            });
    }]);